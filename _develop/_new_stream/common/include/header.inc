<div class="hd_box">
  <div class="row">
    <h1 class="hd_box_logo"><a href=""><img src="/common/images/logo.png" alt="Logo"></a></h1>
    <div id="humberger" class="humberger show_sp">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <!--/.humberger-->
    <nav class="hd_box_nav" id="nav">
      <ul>
        <li><a href="http://www.racoo.co.jp/test/newstream/html/about.html">会社概要</a></li>
        <li><a href="http://www.racoo.co.jp/test/newstream/html/contact.html">お問い合わせ</a></li>
        <li><a href="http://www.racoo.co.jp/test/newstream/html/privacypolicy.html">プライバシーポリシー</a></li>
        <li class="show_sp"><a href="/#scene_02" class="anchor">イベント企画・運営事業</a></li>
        <li class="show_sp"><a href="/#studio" class="anchor">レンタルスタジオ運営事業</a></li>
        <li class="show_sp"><a href="/#contract" class="anchor">請負事業</a></li>
        <li class="show_sp"><a href="/#business" class="anchor">コンサルティング事業</a></li>
        <li class="show_sp"><a href="/#production_sp" class="anchor">プロダクション事業</a></li>
      </ul>
      <ul class="show_pc">
        <li><a href="/#des_event" class="anchor">イベント企画・運営事業</a></li>
        <li><a href="/#studio" class="anchor">レンタルスタジオ運営事業</a></li>
        <li><a href="/#consulting" class="anchor">請負事業</a></li>
        <li><a href="/#consulting" class="anchor">コンサルティング事業</a></li>
        <li><a href="/#production" class="anchor">プロダクション事業</a></li>
      </ul>
    </nav>
    <!--./nav-->
  </div>
</div>
<div class="hd_side">
    <div class="hd_side_scroll">
        <span class="txt">SCROLL</span>
    </div>
    <div class="hd_side_bar">
        <span id="js-current-bar" class="current">abc</span>
    </div>
</div>