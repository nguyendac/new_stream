function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
function isSpecial(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top <= 0));
}

var Scene01 = (function(){
  function Scene01(){
    var s = this;
    this.sY = 0;
    this.lY = 0;
    this._buffer = 30;
    new Leaf('leaf_01',-3,-100);
    new Leaf('leaf_02',99,-50);
  }
  return Scene01;
})()
var Leaf = (function(){
  function Leaf(target,left,top){
    var l = this;
    this._target = document.getElementById(target);
    this._scene_left = document.getElementById('scene_01');
    this.bgLeaf = document.getElementById('posLeaf');
    this._srcScene = '/scene_01/images/scene1_bg.jpg';
    this._img = new Image();
    this._img.onload = function(){

    }
    this._img.src = this._srcScene;

    this.sY = 0;
    this.rotate = getRandomArbitrary(-60,60);
    this.handling = function(){
      if(isPartiallyVisible(l._scene_left)) {
        var _top  = document.documentElement.scrollTop || document.body.scrollTop;
        var t =  (_top - l._scene_left.offsetTop) *(window.innerHeight-top)/(l._scene_left.clientHeight);
        var buffer_left = getRandomArbitrary('-10,10');
        var posT = getPosition(l._scene_left).y*window.innerHeight/l._scene_left.clientHeight;
        l._target.style.left = left+buffer_left+'px';
        l._target.style.top = t+'px';
        l.rotate+=0.1 ;
        l._target.style[transformProperty] = "rotate("+l.rotate+"deg)";
        l.bgLeaf.style.backgroundPosition = "center "+(-t/3)+"px";
      }
    }

    // this.handling();
    window.addEventListener('scroll',l.handling,false);
  }
  return Leaf;
})()
var Fish = (function(){
  function Fish(){
    var f = this;
    this._scene = document.getElementById('scene_02');
    this._posBg = document.getElementById('posFall');
    this.handling = function(){
      if(isPartiallyVisible(f._scene)) {
        var _top  = document.documentElement.scrollTop || document.body.scrollTop;
        var t =  (_top - f._scene.offsetTop) *(window.innerHeight)/(f._scene.clientHeight);
        if(t > 0) {
          f._posBg.style.backgroundPosition = "center bottom "+(-t/3)+"px";
        }

      }
    }
    this.handling();
    window.addEventListener('scroll',f.handling,false);
  }
  return Fish;
})()
var Bird = (function(){
  function Bird(){
    var b = this;
    this._target = document.getElementById('bird');
    this._scene_left = document.getElementById('scene_03');
    this._posBg = document.getElementById('posRiver');
    this.pBot = 0;
    // flying
    this._img = this._target.querySelector('img');
    // this._posBg = document.getElementById('posFall');
    this._scene_left.style.bacgroundSize = this._scene_left.clientWitdh+"px "+ +this._scene_left.clientHeight+"px";
    this._c = 0;
    this._delay = 0;
    this.images = [
      "/scene_03/images/bird_01.png",
      "/scene_03/images/bird_02.png",
      "/scene_03/images/bird_03.png",
      "/scene_03/images/bird_04.png",
      "/scene_03/images/bird_05.png",
      "/scene_03/images/bird_06.png"
    ];
    this._fly =  function(){
      b._delay+=1;
      if(b._delay > 20) {
        b._img.src = b.images[b._c];
        b._c++;
        if(b._c >= b.images.length) {
          b._c = 0;
        }
        b._delay = 0;
      }
      window.requestAnimFrame(b._fly);
    }
    this._fly();
    // end flying
    this.handling = function(){
      if(isPartiallyVisible(b._scene_left)) {
        var _top  = document.documentElement.scrollTop || document.body.scrollTop;
        var t =  (_top - b._scene_left.offsetTop) *(window.innerHeight)/(b._scene_left.clientHeight);
        b._target.style.bottom = parseInt(b.pBot + t)+'px';
        if(t > 0) {
          b._posBg.style.backgroundPosition = "center bottom "+(-t/2)+"px";
        }
      }
    }
    this.handling();
    window.addEventListener('scroll',b.handling,false);
  }
  return Bird;
})()
var Savanah = (function(){
  function Savanah(){
    var s = this;
    this._target = document.getElementById('savanah');
    this._target2 = document.getElementById('savanah02');
    this._scene = document.getElementById('scene_04');
    this._posBg = document.getElementById('posSavanah');
    this._h = this._scene.clientHeight;
    this.handling = function(){
      if(isPartiallyVisible(s._scene)) {
        var _top  = document.documentElement.scrollTop || document.body.scrollTop;
        var t =  (_top - s._scene.offsetTop) *(window.innerHeight)/(s._scene.clientHeight);
        // var posBg = getPosition(s._scene).y*(window.innerHeight)/(s._scene.clientHeight)+20;
        if(t > 0) {
          s._posBg.style.backgroundPosition = "center top "+(-t/3)+"px";
        }
        s._target.style.right = t*3.5+'px';
        s._target2.style.right = t*3.5 - 1200+'px';
      }
      s._target.style[transformProperty] = "translateY("+((s._h/3.5) + t + 400)+"px)";
      s._target2.style[transformProperty] = "translateY("+((s._h/3.5) + t + 1000)+"px)";
    }
    this.handling();
    window.addEventListener('scroll',s.handling,false);
  }
  return Savanah;
})()
var Airship = (function(){
  function Airship(){
    var s = this;
    this._target = document.getElementById('airship');
    this._target2 = document.getElementById('airship02');
    this._scene = document.getElementById('scene_05');
    this._posBg = document.getElementById('posShip');
    this._h = this._scene.clientHeight;
    this.handling = function(){
      if(isPartiallyVisible(s._scene)) {
        var _top  = document.documentElement.scrollTop || document.body.scrollTop;
        var t =  (_top - s._scene.offsetTop) *(window.innerHeight)/(s._scene.clientHeight);
        s._target.style.right = parseInt(s.pBot + t)+'px';
        var posBg = getPosition(s._scene).y*(window.innerHeight)/(s._scene.clientHeight)+20;
        if(t > 0) {
          s._posBg.style.backgroundPosition = "center top "+(-t/2 - 50)+"px";
        }
        s._target.style.right = t*3.5+'px';
        s._target2.style.right = t*3.5 - 2000+'px';
      }
      // s._target.style[transformProperty] = "translateY("+(-getPosition(s._scene).y)+"px)";
      s._target.style[transformProperty] = "translateY("+((s._h/3.5) + t )+"px)";
      s._target2.style[transformProperty] = "translateY("+((s._h/3.5) + t + 1500)+"px)";
    }
    this.handling();
    window.addEventListener('scroll',s.handling,false);
  }
  return Airship;
})()
var Firework = (function(){
  function Firework(){
    var s = this;
    this._target = document.getElementById('firework');
    this._scene = document.getElementById('scene_06');
    this._posBg = document.getElementById('posFire');
    this.steps = this._scene.querySelectorAll('.step');
    this._slogan = document.getElementById('slogan');
    this._target_left = document.getElementById('firework_left');
    this._target_right = document.getElementById('firework_right');
    this._target_left_sp = document.getElementById('firework_left_sp');
    this._target_right_sp = document.getElementById('firework_right_sp');
    this.flg_s3 = document.getElementById('flg_s3');
    this.flg_s5 = document.getElementById('flg_s5');
    this.c = 0;
    this._delay = 0;
    this._delayl = 0;
    this._delayr = 0;
    this._delay_left_sp = 0;
    this._delay_right_sp = 0;
    this._timer;
    this.handling = function(){
      if(isPartiallyVisible(s._scene)) {
        var _top  = document.documentElement.scrollTop || document.body.scrollTop;
        var t =  (_top - s._scene.offsetTop) *(window.innerHeight)/(s._scene.clientHeight);
        var ts = getPosition(s._scene).y/100;
        if(t > 0) {
          if(window.innerWidth < 1200) {
            s._posBg.style.backgroundPosition = "center top "+(ts)+"px";
          } else {
            s._posBg.style.backgroundPosition = "center top "+(-t/5)+"px";
          }
        }
      }
      if(getPosition(s._slogan).y <= 0){
        s._slogan.classList.add('start');
        // s._target.classList.add('active');
        if(!s._target.classList.contains('action')) {
          s._handf1();
        }
        setTimeout(function(){
          s._slogan.classList.add('end');
        },2000);
      } else {
        s._slogan.classList.remove('start');
        s._slogan.classList.remove('end');
      }
      if(getPosition(s.flg_s3).y <= window.innerHeight/2){
        if(!s._target_left.classList.contains('action')) {
          s._handfl();
        }
      }
      if(getPosition(s.flg_s5).y <= window.innerHeight/2){
        if(!s._target_right.classList.contains('action')) {
          s._handfr();
        }
      }
    }
    this._handf1 = function(){
      s._delay+=1;
      if(s._delay == 1) {
        s._target.classList.add('active');
        s._target.classList.add('action');
      }
      if(s._delay == 120) {
        s._target.classList.remove('active');
      }
      if(s._delay > 240) {
        s._delay = 0;
      }
      window.requestAnimFrame(s._handf1);
    }
    this._handfl = function(){
      s._delayl+=1;
      if(s._delayl == 1) {
        s._target_left.classList.add('active');
        s._target_left.classList.add('action');
      }
      if(s._delayl == 120) {
        s._target_left.classList.remove('active');
      }
      if(s._delayl > 240) {
        s._delayl = 0;
      }
      window.requestAnimFrame(s._handfl);
    }
    this._handfr = function(){
      s._delayr+=1;
      if(s._delayr == 1) {
        s._target_right.classList.add('active');
        s._target_right.classList.add('action');
      }
      if(s._delayr == 120) {
        s._target_right.classList.remove('active');
      }
      if(s._delayr > 240) {
        s._delayr = 0;
      }
      window.requestAnimFrame(s._handfr);
    }
    this._hand_left_sp = function(){
      s._delay_left_sp+=1;
      if(s._delay_left_sp == 1) {
        s._target_left_sp.classList.add('active');
        s._target_left_sp.classList.add('action');
      }
      if(s._delay_left_sp == 120) {
        s._target_left_sp.classList.remove('active');
      }
      if(s._delay_left_sp > 200) {
        s._delay_left_sp = 0;
      }
      window.requestAnimFrame(s._hand_left_sp);
    }
    this._hand_right_sp = function(){
      s._delay_right_sp+=1;
      if(s._delay_right_sp == 1) {
        s._target_right_sp.classList.add('active');
        s._target_right_sp.classList.add('action');
      }
      if(s._delay_right_sp == 120) {
        s._target_right_sp.classList.remove('active');
      }
      if(s._delay_right_sp > 200) {
        s._delay_right_sp = 0;
      }
      window.requestAnimFrame(s._hand_right_sp);
    }
    this.handling();
    this._hand_left_sp();
    setTimeout(s._hand_right_sp,1600);
    window.addEventListener('scroll',s.handling,false);
  }
  return Firework;
})()
var Effect = (function(){
  function Effect(){
    var ef = this;
    this.eles = document.querySelectorAll('.effect');
    this.half = document.querySelectorAll('.half');
    this.full = document.querySelectorAll('.full');
    this.handling = function(){
      Array.prototype.forEach.call(ef.eles,function(el,i){
        if(isPartiallyVisible(el)){
          el.classList.add('active');
        }
      })
      Array.prototype.forEach.call(ef.half,function(h,k){
        if(isPartiallyVisible(h)) {
          h.classList.add('active');
        }
      })
      Array.prototype.forEach.call(ef.full,function(f,l){
        if(isFullyVisible(f)) {
          f.classList.add('active');
        }
      })
    }
    window.addEventListener('scroll',ef.handling,false);
    this.handling();
  }
  return Effect;
})()
var ScrollSection = (function(){
  function ScrollSection(){
    var s = this;
    this.timer;
    this.flag_start = true;
    this._basic = window.innerHeight;
    // this._start = document.getElementById('start');
    this._targets = document.querySelectorAll('.common_top');
    this._length = this._targets.length;
    this.lastscroll = 0;
    this._ua = navigator.userAgent.toLowerCase();
    this._curr = (Math.ceil(window.scrollY/this._basic) >= this._length) ? this._length-1:Math.ceil(window.scrollY/this._basic);
    this.preventDefault = function(e) {
      e = e || window.event;
      if (e.preventDefault)
        e.preventDefault();
      e.returnValue = false;
    }
    this.disablescroll = function(){
      if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', s.preventDefault, false);
      window.onwheel = s.preventDefault; // modern standard
      window.onmousewheel = document.onmousewheel = s.preventDefault; // older browsers, IE
    }
    this.enablescroll = function(){
      if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', s.preventDefault, false);
      window.onmousewheel = document.onmousewheel = null;
      window.onwheel = null;
    }
    this._detectsafari = function(){
      if (s._ua.indexOf('safari') != -1) {
        if (s._ua.indexOf('chrome') > -1) {
          return true;
        } else {
          return false;
        }
      }
    }
    this.scrollToY = function(scrollTargetY,speed,easing){
      var scrollY = window.scrollY,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;
      var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));
      function tick() {
        if(s.flag_start){
          currentTime += 1 / 60;
          var p = currentTime / time;
          var t = easingEquations[easing](p);
          if (p < 1) {
            requestAnimFrame(tick);
            s.disablescroll();
            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
          } else {
            setTimeout(function(){
              s.enablescroll();
            },1000);
            window.scrollTo(0, scrollTargetY);
          }
        }
      }
      tick();
    }
    this.handling = function(e){
      var delta = e.wheelDelta ? e.wheelDelta:-e.detail;
      if(delta > 0 ) {
        if(s._curr > 0 && s._curr <= s._length - 1) {
          var curr = s._curr;
          setTimeout(function(){
            s._targets[curr].classList.remove('active');
          },500);
          s._curr-=1;
          s.scrollToY(s._targets[s._curr].offsetTop,500,'easeOutSine');
        }
      } else {
        if(s._curr < s._length-1) {
          var curr = s._curr;
          setTimeout(function(){
            s._targets[curr].classList.remove('active');
          },500);
          s._curr+=1;
          s.scrollToY(s._targets[s._curr].offsetTop,500,'easeOutSine');
        } else {
          s._curr = s._length-1;
          return;
        }
      }
    }
    this.scrollToY(s._targets[s._curr].offsetTop,500,'easeOutSine');
    window.addEventListener('mousewheel',s.handling,false);
    window.addEventListener('DOMMouseScroll',s.handling,false);
    this._timer;
    window.addEventListener('scroll',function(e){
      if(s._detectsafari) {
        clearTimeout(s._timer);
        s._curr = Math.ceil(this.scrollY/s._basic);
        if(s._curr > s._length-1) {
          return;
        }
        s._timer = setTimeout(function(){
          s.scrollToY(s._targets[s._curr].offsetTop,500,'easeOutSine')
        },100);
      }
    });
    window.addEventListener('resize',function(){
      s._basic = window.innerHeight;
    })
  }
  return ScrollSection;
})()
var Scene = (function(){
  function Scene(){
    var ef = this;
    this.eles = document.querySelectorAll('.scene');
    this.backdore = document.getElementById('backdore');
    this.hd_box = document.getElementById('header').querySelector('.hd_box');
    this.timer == null;
    this.handling = function(){
      Array.prototype.forEach.call(ef.eles,function(el,i){
        if(getPosition(el).y <= 0){
          ef.backdore.classList.add(el.dataset.bg);
        } else {
          ef.backdore.classList.remove(el.dataset.bg);
        }
      })
      ef.stopScroll();
    }
    this.stopScroll = function(){
      if(ef.timer !== null) {
        clearTimeout(ef.timer);
        ef.hd_box.classList.add('hidden');
      }
      ef.timer = setTimeout(function(){
        ef.hd_box.classList.remove('hidden');
      },1000);
    }
    window.addEventListener('scroll',ef.handling,false);
    this.handling();
  }
  return Scene;
})()
var Scene2 = (function(){
  function Scene2(){
    var s = this;
    this._target = document.getElementById('scene_02');
    this.steps = this._target.querySelectorAll('.quarter');
    this.event = document.getElementById('event');
    this.fish = document.getElementById('fish');
    this.images = [
      "/scene_02/images/fish_01.png",
      "/scene_02/images/fish_02.png",
      "/scene_02/images/fish_03.png",
      "/scene_02/images/fish_02.png",
      "/scene_02/images/fish_01.png",
    ];
    this._img = this.fish.querySelector('img');
    this.handling = function(){
      var c = 0;
      Array.prototype.forEach.call(s.steps,function(el,i){
        if(isFullyVisible(el)) {
          s._img.src = s.images[i];
          el.classList.add('active');
          s.fish.classList.add('step_0'+(i));
          if(i == s.images.length - 1) {
            s.event.classList.add('active');
          }
        }
      })
    }
    window.addEventListener('scroll',s.handling,false);
  }
  return Scene2;
})();
var Bar = (function(){
  function Bar(){
    var b = this;
    this._target = document.getElementById('js-current-bar');
    this._main =  document.querySelector('main');
    this._h = this._main.clientHeight;
    this._h_wrap = this._target.parentNode.clientHeight;
    this.handling = function(){
      var _t = Math.abs(getPosition(b._main).y)*b._h_wrap/b._h;
      b._target.style.height = _t+'px';
    }
    this.handling();
    window.addEventListener('scroll',b.handling,false);
  }
  return Bar;
})()
var Ab = (function(){
  function Ab(){
    var a = this;
    this._target = document.getElementById('absolute');
    this._obj = document.getElementById('scene_06');
    this._handling = function(){
      var _top = getPosition(a._obj).y;
      if(_top < 0 && window.innerWidth < 769) {
        // a._target.style.top = (-_top)+'px';
        // a._target.style[transformProperty] = "translateY("+(-_top)+"px)";
        // console.log(_top/a._obj.clientHeight);
        a._target.style.top = -_top+'px';
      }
    }
    window.addEventListener('scroll',a._handling,false);
  }
  return Ab;
})()
var Pos = (function(){
  function Pos(){
    var p = this;
    this._eles = document.querySelectorAll('.scene');
    this._header = document.getElementById('header').querySelector('.hd_box');
    this._pos = document.querySelectorAll('.pos');
    this._handle = function(){
      var _top  = document.documentElement.scrollTop || document.body.scrollTop;
      Array.prototype.forEach.call(p._eles,function(el,i){
        if(getPosition(el).y < 0) {
          p._pos[i].classList.add('fixed');
          p._pos[i].style.top = 0;
        } else {
          p._pos[i].classList.remove('fixed');
          p._pos[i].style.top = el.offsetTop+p._header.clientHeight+'px';
        }
        if(getPosition(el).y + el.clientHeight < 0) {
          p._pos[i].classList.remove('fixed');
          p._pos[i].style.top = el.offsetTop+p._header.clientHeight+'px';
        }

      })
    }
    this._handle();
    window.addEventListener('scroll',p._handle,false);
  }
  return Pos;
})()
var Anchor = (function(){
  function Anchor(){
    var a = this;
    this._target = '.anchor';
    this._header = document.getElementById('header').querySelector('.hd_box');
    this.timer;
    this.flag_start = false;
    this.iteration;
    this.eles = document.querySelectorAll(this._target);
    this.stopEverything = function(){
      a.flag_start = false;
    }
    this._getbuffer = function() {
      var _buffer = 0;
      if(window.innerWidth < 768) {
        _buffer = a._header.clientHeight;
      }
      return _buffer;
    }
    this.scrollToY = function(scrollTargetY,speed,easing){
      var scrollY = window.scrollY || window.pageYOffset,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;
      var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));
      function tick() {
        if(a.flag_start){
          currentTime += 1 / 60;
          var p = currentTime / time;
          var t = easingEquations[easing](p);
          if (p < 1) {
            requestAnimFrame(tick);
            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
          } else {
            window.scrollTo(0, scrollTargetY);
          }
        }
      }
      tick();
    }
    Array.prototype.forEach.call(this.eles,function(el,i){
      el.addEventListener('click',function(e){
        var next = el.getAttribute('href').split('#')[1];
        if(document.getElementById(next)){
          // e.preventDefault();
          // a.scrollToY((document.getElementById(next).offsetTop - a._getbuffer()),1500,'easeOutSine');
          if(window.innerWidth < 769) {
            document.getElementById('humberger').click();
          }
        }
      })
    });
    document.querySelector("body").addEventListener('mousewheel',a.stopEverything,false);
    document.querySelector("body").addEventListener('DOMMouseScroll',a.stopEverything,false);
  }
  return Anchor;
})()
var Bird_sp = (function(){
  function Bird_sp(){
    var b = this;
    this._target = document.getElementById('bird_sp');
    this._img = this._target.querySelector('img');
    this._c = 0;
    this._delay = 0;
    this.images = [
      "/scene_03/images/bird_01.png",
      "/scene_03/images/bird_02.png",
      "/scene_03/images/bird_03.png",
      "/scene_03/images/bird_04.png",
      "/scene_03/images/bird_05.png",
      "/scene_03/images/bird_06.png"
    ];
    this._fly =  function(){
      b._delay+=1;
      if(b._delay > 20) {
        b._img.src = b.images[b._c];
        b._c++;
        if(b._c >= b.images.length) {
          b._c = 0;
        }
        b._delay = 0;
      }
      window.requestAnimFrame(b._fly);
    }
    this._fly();
  }
  return Bird_sp;
})()
var Fish_sp = (function(){
  function Fish_sp(){
    var f = this;
    this._target = document.getElementById('fish_sp');
    this._img = this._target.querySelector('img');
    this._c = 0;
    this._delay = 0;
    this.images = [
      "/scene_02/images/scene2_img_sp_01.png",
      "/scene_02/images/scene2_img_sp_02.png",
      "/scene_02/images/scene2_img_sp_03.png",
      "/scene_02/images/scene2_img_sp_01.png",
      "/scene_02/images/scene2_img_sp_02.png",
      "/scene_02/images/scene2_img_sp_03.png"
    ];
    this._swim =  function(){
      f._delay+=1;
      if(f._delay > 20) {
        f._img.src = f.images[f._c];
        f._c++;
        if(f._c >= f.images.length) {
          f._c = 0;
        }
        f._delay = 0;
      }
      window.requestAnimFrame(f._swim);
    }
    this._swim();
  }
  return Fish_sp;
})()
window.addEventListener('DOMContentLoaded',function(){
  new Fish_sp();
  new Bird_sp();
  new Anchor();
  new Pos();
  new Ab();
  new Bar();
  new Firework();
  new Airship();
  new Savanah();
  new Scene2();
  new Effect();
  new Scene();
  new Scene01();
  new Bird();
  new Fish();
})