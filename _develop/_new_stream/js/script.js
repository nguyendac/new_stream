function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
window.addEventListener('DOMContentLoaded',function(){
  new Scene01();
  new Opacity();
  new Rtl();
})
var Leaf = (function(){
  function Leaf(target,left,top){
    var l = this;
    this._target = document.getElementById(target);
    this._scene_left = document.getElementById('scene_01');
    this.sY = 0;
    this.rotate = getRandomArbitrary(-60,60);
    this.handling = function(){
      if(isPartiallyVisible(l._scene_left)) {
        var _top  = document.documentElement.scrollTop || document.body.scrollTop;
        var t =  (_top - l._scene_left.offsetTop) *(window.innerHeight-top)/(l._scene_left.clientHeight);
        var buffer_left = getRandomArbitrary('-10,10');
        l._target.style.left = left+buffer_left+'px';
        l._target.style.top = t+'px';
        l.rotate+=0.1 ;
        l._target.style[transformProperty] = "rotate("+l.rotate+"deg)";
      }
    }
    window.addEventListener('scroll',l.handling,false);
  }
  return Leaf;
})()
var Opacity = (function(){
  function Opacity(){
    var o = this;
    this._eles = document.querySelectorAll('.opacity');
    this.handling = function(){
      Array.prototype.forEach.call(o._eles,function(el){
        if(getPosition(el).y > 0 && (getPosition(el).y) < window.innerHeight) {
          el.style.opacity = (window.innerHeight - getPosition(el).y)/el.clientHeight;
        }
        if(getPosition(el).y < 0 && getPosition(el).y > window.innerHeight) {
          el.style.opacity = 0;
        } 
      })
    }
    this.handling();
    window.addEventListener('scroll',o.handling,false);
  }
  return Opacity;
})()
var Rtl = (function(){
  function Rtl(){
    var r = this;
    this._eles = document.querySelectorAll('.flag_rtl');
    this.handling = function(){
      Array.prototype.forEach.call(r._eles,function(el){
        var trx = getPosition(el).y*el.clientWidth/el.clientHeight;
        if(getPosition(el).y > 0 && (getPosition(el).y) < window.innerHeight) {
          el.querySelector('.rtl').classList.add('active');
          el.querySelector('.rtl').style.right = -trx+"px";
        }
        if(getPosition(el).y > window.innerHeight) {
          el.querySelector('.rtl').style.opacity = 1;
          el.querySelector('.rtl').style.right = -el.clientWidth+"px";
        } 
        if(getPosition(el).y < 0) {
          el.querySelector('.rtl').style.right = -trx+"px";
          if(-trx >= (window.innerWidth -1000)/2) {
            el.querySelector('.rtl').style.right = (window.innerWidth - 1000)/2+'px';
            el.querySelector('.rtl').classList.remove('active');
          }
          // el.querySelector('.rtl').style.right = (window.innerWidth - 1000)/2+'px';
          // el.querySelector('.rtl').style[transformProperty] = "translateX(0px)";
          // el.querySelector('.rtl').classList.remove('active');
        }
      })
    }
    this.handling();
    window.addEventListener('scroll',r.handling,false);
  }
  return Rtl;
})()
var Scene01 = (function(){
  function Scene01(){
    var s = this;
    this.sY = 0;
    this.lY = 0;
    this._buffer = 30;
    new Leaf('leaf_01',-3,-100);
    new Leaf('leaf_02',99,-50);
  }
  return Scene01;
})()