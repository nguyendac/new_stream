window.addEventListener('DOMContentLoaded',function(){
  new Effect();
})
var Effect = (function(){
  function Effect(){
    var ef = this;
    this.eles = document.querySelectorAll('.effect');
    this.half = document.querySelectorAll('.half');
    this.handling = function(){
      Array.prototype.forEach.call(ef.eles,function(el,i){
        if(isFullyVisible(el)){
          el.classList.add('active');
        }
      })
      Array.prototype.forEach.call(ef.half,function(h,k){
        if(isPartiallyVisible(h)) {
          h.classList.add('active');
        }
      })
    }
    window.addEventListener('scroll',ef.handling,false);
    this.handling();
  }
  return Effect;
})()